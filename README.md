# wprint

a quick and dirty shell script for taking screenshots on wayland and placing saving them onto your disk or your clipboard.

# dependencies

- grim
- slurp
- wl-clipboard 

# install

Just run `./wprint` and it should run the command just fine.
If you want to run it from a compositor or as a macro, you should probably move the script to somewhere in your `$PATH` like `/usr/bin/` or `/usr/local/bin`

To use *wprint* with dwl, you can use my [patched version](https://codeberg.org/defal/defalwl/)
or you can patch it manually by install it somewhere in your `$PATH` and then add a pointer command to your `config.h`:
```
/* commands */
static const char *termcmd[] = { "foot", NULL };
static const char *menucmd[] = { "bemenu-run", NULL };
static const char *print[] = { "wprint", NULL };
```

also add this line inside your `keys[]`
like so:
```
static const Key keys[] = {
    ...your stuff here

    { 0,			    XKB_KEY_Print,	spawn,		{.v = print } },
    ...
}
```

# options

TODO

# credits

Zircon for the original version of this script, which helped me alot.
All of the creators of such amazing software used in this project.
